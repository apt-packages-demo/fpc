# fpc

Free Pascal - SDK suite dependency package. https://tracker.debian.org/pkg/fpc

## Official documentation
* [*CGI Web Programming*](https://wiki.freepascal.org/CGI_Web_Programming)

## Unofficial documentation
* [pascal file extension](https://google.com/search?q=pascal+file+extension)
* [*File extensions*](https://wiki.lazarus.freepascal.org/File_extensions)
* [*Free Pascal*](https://en.m.wikipedia.org/wiki/Free_Pascal)
* [*Modern Object Pascal Introduction for Programmers*
  ](https://castle-engine.io/modern_pascal_introduction.html)
  (2018) Michalis Kamburelis